# AssetCreator

## Description
UI to configure how the usd asset has to be initialized and what layers to create.\
```
|-------------Create Asset--------------|
|       Project: StarWars               |
|    Asset Name: DarthVader             |
|    Asset Type: Character              |
|---------------------------------------|
|  [X] Model                            |
|  [X] Look                             |
|  [ ] Rig                              |
|  [ ] Anim                             |
|  [ ] Groom                            |
|  [ ] FX                               |
|  [ ] Lights                           |
|                                       |
|---------------------------------------|
|    [ Cancel ]      [ Create/Update ]  |
|---------------------------------------|
UI mockup
```

## TODO
 - Add linting with nox. i need to configure the linters to produce pretty code.
 - rnd how to generate run the nox stuff in gitlab ci/cd
 - Add tests with nox. just something simple to test how to use tox.



## Installation
To install just clone the repo and run the installer.\
Make sure you have python installed and added to the PATH
```
git clone https://gitlab.com/TeideTools/assetcreator.git
cd assetcreator
call install.bat
```
(deprecated) This will just create a venv inside the repo, and install the dependencies into it.
Now you should be able to open the tool with [openApp.bat](openApp.bat)

This will build the wheel that can be later installed with pip install name.whl


## Roadmap
I want to expand this to be used for assembly assets. Add composition templates for each asset-type,\
and explore options for shot/sequence level manipulation.
- Support Assembly assets (FruitBowl = Bowl + Apple)
- Edit-template mode
- Expand tool to be able to work on shots/sequences. (not soon)

## Authors and acknowledgment
This is a toy project made by me on my free time. I wanted to do something like this for a while...\
But I got motivated again after I saw a [post](https://forum.aousd.org/t/building-an-asset-pipeline/701) from [Raymond](https://forum.aousd.org/u/Raymond) in the AOUSD forums.

## License
For open source projects, [MIT License](https://opensource.org/license/mit/).

## Project status
We have a starting point to make these UI more sophisticated and improve the UX.
This is the ImagePicker inside the AssetCreator and the AssetBrowser.
![Current state of AssetCreator and AssetBrowser](resources/preview.png)

