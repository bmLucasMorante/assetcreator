@ECHO OFF

rem Create | Activate | Update the virtual environment
python -m venv .venv

call ".venv\Scripts\activate" && (
    python -m pip install --upgrade pip
    rem Build & install package
    pip install -r requirements-dev.txt
)
