import nox

@nox.session(python="3.9")
def setup(session):
    """Setup the environment and run the application."""
    session.install(".")
    session.run("python", "-m", "assetCreator")

@nox.session(python="3.9")
def tests(session):
    """Run the tests."""
    session.install(".")
    session.install("pytest")
    session.run("pytest")

@nox.session(python="3.9")
def lint(session):
    """Lint the code."""
    session.install("flake8", "pylint", "mypy", "isort")
    session.run("flake8", "src")
    session.run("pylint", "src")
    session.run("mypy", "src")
    session.run("isort", "--check-only", "src")

@nox.session(python="3.9")
def build(session):
    """Build the package."""
    session.install("setuptools", "wheel")
    session.run("python", "setup.py", "sdist", "bdist_wheel")

@nox.session(python="3.9")
def format(session):
    """Format the code."""
    session.install("black")
    session.run("black", "src")