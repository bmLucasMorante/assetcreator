"""
Copyright 2023 Lucas Morante

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


AssetCreator Tool

UI to configure how the asset has to be initialized and what layers to create.

|-------------Create Asset--------------|    |-------------Create Asset--------------|
|       Project: StarWars               |    |       Project: Library                |
|    Asset Name: DarthVader             |    |    Asset Name: Flashlight             |
|    Asset Type: Character              |    |    Asset Type: Prop                   |
|---------------------------------------|    |---------------------------------------|
|  [X] Model                            |    |  [X] Model                            |
|  [X] Look                             |    |  [X] Look                             |
|  [ ] Rig                              |    |  [X] Rig                              |
|  [ ] Anim                             |    |  [ ] Anim                             |
|  [ ] Groom                            |    |  [ ] Groom                            |
|  [ ] FX                               |    |  [ ] FX                               |
|  [ ] Lights                           |    |  [X] Lights                           |
|                                       |    |                                       |
|---------------------------------------|    |---------------------------------------|
|    [ Cancel ]      [ Create/Update ]  |    |    [ Cancel ]      [ Create/Update ]  |
|---------------------------------------|    |---------------------------------------|

Asset_name: start with capital letter
Asset_type: pick one from [Prop, Char, Set ...]

layers: Each asset_type has different default enabled layers
    - Model: load/define geo.
        Could we bring in this layer other assets? (FruitBowl = Bowl + FruitA + FruitB)
    - Look: load/create mats and define assignments.
    - Rig: maya rig and can also be a usdSkel?
    - Anim: Asset level animation cycles
        ex: spinning fan, grass wind
    - Groom: (cfx stuff)
    - FX: Asset level cycles
        ex: smoke/fire in a fireplace, chimney
    - Lights: Asset related lights
        ex: lights in a car/flashlight/lamp

"""
import re
import sys
import webbrowser
from typing import Optional

import qtawesome

from qtpy import QtCore, QtGui, QtWidgets
from widgets.imagePicker import ImagePickerWidget

# done: add thumbnail preview
# done: [next] move ImagePickerWidget to widgets.
# todo: [next] make AssetWidget to be used in grid.
# todo: [next] disable create button if comboboxes are in default value
# todo: implement model view
# todo: load combobox found_values from somewhere else
# todo: [later] make a data model that stores a layer graph template per asset-type
# todo: [later] add usd variants section (pipe defined variantSets: mod, look)
# done: [later] add description section (provide usd metadata)
# todo: [later] add thumbnail? --more like an update action. (usdModelAPI)
# done: [later] improve thumbnail window by adding floating buttons
#       If no thumbnail:
#       - big/center button 'Add thumbnail'
#       If thumbnail:
#       - big/center thumbnail view
#       - corner left-up floating button 'remove thumbnail'
#       - corner right-up floating button 'change thumbnail'
# todo: [R&D] should I use Luma's usd-qt ?
# todo: [R&D] Do we want to represent each department or focus on the usd layers
# todo: [R&D] How can this be expanded to be used for shots/sequences
# todo: [R&D] re-think UI layout -- center layer checkboxes, thumbnail window, tabs
# todo: [R&D] Add a new tab for an Edit layer graph by asset-type
#       I want to explore the idea to have a graph to represent an asset-type.
# todo: [R&D] How to author Assembly assets (Fruitbowl = FruitA + Bowl)
#        - I can't control how the assets are gonna get composed.
#        ex: sometimes can be referenced and other times loaded into a pointInstancer.
#        And we dont have any access to the prim that is gonna be referenced/payloaded.
#        - I do want to have a metadata-like way of saying this asset uses that asset
#        but without actually making the references. We could store this as some kind of metadata
#        in the Fruitbowl.usd layer
#        - We could add a new section, child-assets, with a list with already existing asset-names
#                   Child Assets [Bowl, FruitA, FruitB]  [+]
#                     QLabel      QListEdit(readOnly)  QPushButton
#        The Push button will open the assetBrowser, but for now we can unlock
#        the QListEdit and just type the asset names; we will need a validator
#        to check that the assets exist and display the errors nicely.
#        it would be great to be able to highlight/color the bad-typed / non-existing assets

HELP_DOCS = 'https://forum.aousd.org/t/building-an-asset-pipeline/701/6'


def launch_wiki():
    webbrowser.open(HELP_DOCS)


class AssetCreator(QtWidgets.QMainWindow):
    window_name = 'Asset Creator'
    window_icon = None
    minimum_window_sizeH = 300

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.init_ui()
        self.populate_ui()
        self.connect_signals()

        # Window Size: [>minimum_window_sizeH, Minimum] Non-resizeable
        # self.setFixedSize(self.sizeHint())
        # self.setMinimumWidth(self.minimum_window_sizeH)

    def init_ui(self) -> None:
        # MAIN WINDOW
        self.setWindowTitle(self.window_name)
        self.setWindowIcon(QtGui.QIcon(self.window_icon))

        self.main_widget = QtWidgets.QWidget(parent=self)
        self.setCentralWidget(self.main_widget)

        self.main_layout = QtWidgets.QVBoxLayout()
        # self.main_layout.setAlignment(QtCore.Qt.AlignTop)
        self.main_widget.setLayout(self.main_layout)

        # MAIN MENU
        self.main_menu = QtWidgets.QMenuBar()
        self.help_menu = self.main_menu.addMenu('&Help')
        self.help_menu.addAction('See Documentation', launch_wiki)
        self.layout().setMenuBar(self.main_menu)

        # PARAMETERS COLUMN
        self.parameters_column_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.parameters_column_layout)

        # thumbnail
        self.thumbnail_widget = ImagePickerWidget()
        self.parameters_column_layout.addWidget(self.thumbnail_widget)

        # PARAMETERS
        self.asset_form_layout = QtWidgets.QGridLayout()
        self.asset_form_layout.setAlignment(QtCore.Qt.AlignTop)
        self.asset_parameters = QtWidgets.QGroupBox(title="Parameters")
        self.asset_parameters.setLayout(self.asset_form_layout)
        self.parameters_column_layout.addWidget(self.asset_parameters)

        # Project
        self.project_label = QtWidgets.QLabel(text="Project:")
        self.project_label.setAlignment(QtCore.Qt.AlignRight)
        self.asset_form_layout.addWidget(self.project_label, 0, 0)

        self.project_combo = QtWidgets.QComboBox(parent=self.asset_parameters)
        self.asset_form_layout.addWidget(self.project_combo, 0, 1)

        # Asset Type
        self.asset_type_label = QtWidgets.QLabel(text="Asset Type:")
        self.asset_type_label.setAlignment(QtCore.Qt.AlignRight)
        self.asset_form_layout.addWidget(self.asset_type_label, 1, 0)

        self.asset_type_combo = QtWidgets.QComboBox(parent=self.asset_parameters)
        self.asset_form_layout.addWidget(self.asset_type_combo, 1, 1)

        # Asset Name
        self.asset_name_label = QtWidgets.QLabel(text="Asset Name:")
        self.asset_name_label.setAlignment(QtCore.Qt.AlignRight)
        self.asset_form_layout.addWidget(self.asset_name_label, 2, 0)

        self.asset_name_edit = QtWidgets.QLineEdit(parent=self.asset_parameters)
        self.asset_name_edit.setPlaceholderText('Name the asset...')
        self.asset_form_layout.addWidget(self.asset_name_edit, 2, 1)

        # Asset-Children
        self.asset_child_label = QtWidgets.QLabel(text="Child Assets:")
        self.asset_child_label.setAlignment(QtCore.Qt.AlignRight)
        self.asset_form_layout.addWidget(self.asset_child_label, 3, 0)

        text_layout = QtWidgets.QHBoxLayout()
        self.asset_child_text = QtWidgets.QLineEdit(parent=self.asset_parameters)
        self.asset_child_text.setPlaceholderText('None')
        self.asset_child_button = QtWidgets.QPushButton(icon=qtawesome.icon("ri.add-circle-line"),
                                                        parent=self.asset_parameters
                                                        )
        self.asset_child_button.setFlat(True)
        text_layout.addWidget(self.asset_child_text)
        text_layout.addWidget(self.asset_child_button)
        self.asset_form_layout.addLayout(text_layout, 3, 1)

        # Asset Description
        self.asset_desc_label = QtWidgets.QLabel(text="Description:")
        self.asset_desc_label.setAlignment(QtCore.Qt.AlignTop)
        self.asset_desc_label.setAlignment(QtCore.Qt.AlignRight)
        self.asset_form_layout.addWidget(self.asset_desc_label, 4, 0)

        self.asset_desc_text = QtWidgets.QTextEdit(parent=self.asset_parameters)
        self.asset_desc_text.setMaximumHeight(40)
        self.asset_form_layout.addWidget(self.asset_desc_text, 4, 1)


        # ASSET LAYERS
        asset_layers = ["Model", "Look", "Rig", "Anim", "Groom", "FX", "Lights"]
        self.asset_checkboxes_layout = QtWidgets.QHBoxLayout()
        # self.asset_checkboxes_layout.setAlignment(QtCore.Qt.AlignJustify)
        self.asset_checkboxes = QtWidgets.QGroupBox(title="Asset Layers")
        self.asset_checkboxes.setLayout(self.asset_checkboxes_layout)
        self.main_layout.addWidget(self.asset_checkboxes)

        self.checkboxes = {}
        for category in asset_layers:
            checkbox = QtWidgets.QCheckBox(text=category, parent=self.asset_checkboxes)
            self.checkboxes[category] = checkbox
            self.asset_checkboxes_layout.addWidget(checkbox)

        # Create buttons
        self.buttons_layout = QtWidgets.QHBoxLayout()

        self.create_update_button = QtWidgets.QPushButton("Create / Update")
        self.create_update_button.setEnabled(False)
        self.buttons_layout.addWidget(self.create_update_button)

        self.main_layout.addLayout(self.buttons_layout)

    def connect_signals(self) -> None:
        # Parameters - project
        self.project_combo.currentTextChanged.connect(self._remove_default_project)
        # Parameters - name
        self.asset_name_edit.textChanged.connect(self._validate_asset_name)
        # Parameters - type
        self.asset_type_combo.currentTextChanged.connect(self._remove_default_type)
        # Parameters - child assets
        self.asset_child_button.clicked.connect(self._selectChildAssets)

    def populate_ui(self) -> None:
        # Parameters - project
        found_projects = ["Library", "StarWars"]
        self.default_project = 'Select project...'
        found_projects.insert(0, self.default_project)
        self.project_combo.addItems(found_projects)

        # Parameters - type
        found_types = ["Prop", "Character", "Environment"]
        self.default_type = 'Select type...'
        found_types.insert(0, self.default_type)
        self.asset_type_combo.addItems(found_types)

    def _validate_checkboxes(self, text: str) -> Optional[bool]:
        """Checks if the name is alphanumeric"""
        pass

    def _validate_asset_name(self, text: str) -> None:
        """Checks if the name is alphanumeric"""
        # todo: make sure the name is unique in the DB.
        pattern = r'^[a-zA-Z][a-zA-Z0-9]*$'
        match = re.match(pattern, text) is not None
        self.create_update_button.setEnabled(match)

    def _remove_default_project(self):
        if self.default_project == self.project_combo.itemText(0):
            self.project_combo.removeItem(0)

    def _remove_default_type(self):
        if self.default_type == self.asset_type_combo.itemText(0):
            self.asset_type_combo.removeItem(0)

    def _selectChildAssets(self):
        from widgets.assetBrowser import AssetBrowser
        browser = AssetBrowser()
        browser.exec_()
        found_assets = 'Apple, Bowl'
        self.asset_child_text.setText(found_assets)

    def collect_parameters(self):
        pass

    def collect_layers(self):
        pass


def main():
    app = QtWidgets.QApplication(sys.argv)
    qtawesome.dark(app)
    win = AssetCreator()
    win.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
