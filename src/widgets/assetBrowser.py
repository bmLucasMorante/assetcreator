"""
AssetBrowser Tool

First of all, we need to establish an Asset definition so we can represent our asset data
in the codebase.

Asset
-Attributes:
    -name: Name of the asset. has to follow naming convention or at least dont have spaces.
    -author: Author or entity that made the asset
    -version: unspecified-- How to version an asset? Do we version the payloads?
    -date: unspecified-- Date of creation vs date of last edit.
    -category: asset type organization category Characters, Props, Vehicles/planes.
    -project: Global Library or Pipe project.
    -tags: just tags like a characteristics... implement wikidata DB 4 tags
    -thumbnail: just a media representation, usually low res 8bit image.
    -payloads: (aka representations) Abstract representation of the asset. There can be
        any number of payloads in an asset. Payloads can have variants. For example:
        -variantSet: variant class of the payload
            -variant: Actual representation of the asset.
        -ExampleA------------------------
        Material(asset)
            mtlx(payload)
            usd(payload)
            ass(payload)
            Textures(payload)(dependency)
                -OptionA------------------------
                LOD(variantSet)
                    1K(variant)
                        --Actual data
                    4K(variant)
                        --Actual data
                -OptionB------------------------
                --Actual data: can be organized with sub-folders for ext or resolution but we dont get that involved.
        -ExampleB------------------------
        Model(asset)
            //What about dcc scenes?
            //What about other dependencies like vdb files?
            Usd(payload)(scenes)
            blender(payload)(scenes)
            maya(payload)(scenes)
            houdini(payload)(scenes) hip and hda
            mtlx(payload)(dependency)
            Vdb(payload)(dependency)
            Textures(payload)(dependency)
                --Actual data
            Obj(payload)(legacy)
            FBX(payload)(legacy)
            Abc(payload)(legacy)
        -ExampleC------------------------
        HDRI(asset)
            Textures(payload)(dependency)
                --Actual data
    -_usedTimes: Following some logging logic, we will store how many times an asset is used
        in production. The rules of what to measure are uncertain at this point.
        The idea is to "rate" assets by how many times they were picked from the browser.
        We shouldn't say that a tree has been used 50k times if we made a forest,
        we have to measure the set of assets used. From the seq/shot publish we will count
        the forest asset but not the trees, rocks that are inside the forest.
        We could also count the times a publish
        contains an asset, this will avoid listing as used if we just added it to the scene
        but then we didn't actually use it or was removed later.
        Downside is: what about publishing a v2, 3, 4 that doesn't mean the asset has been
        used more, just that the shot had more iterations, so maybe we want to measure
        per publish+shot+seq...
    -_favourite: not exactly an asset attribute but the dataclass passed to the UI model
        will use this attribute to enable the star-indicator. The ground truth data will
        be stored in ${usr}/.teide/someFile.ext where we will store a list of fav assets,
        then check that with the assets to set the attribute True/False.
        When we change the fav state of an asset, its just a live-edit(local file is unchanged)
        then when the ui closes, we will update the local fav list. I think.

Now that we have a definition of what an asset is, we can start talking about how the
browser could look like and what features it needs. Main Widgets

AssetTypes: RadioButton behaviour of types like HDRI, MAT, MODEL
    (what about new types like hdas, bifrost graphs .. Those could be Models w/ category HDA, Bifrost)
Filters: Combobox(project) project/lib; search bar; iconSize; sort order
Categories: TreeView with the categories generated from the union of all the assets of assetType X
Details: widget to show the dataclass exposed attrs; TreeView for dictionary data
Actions: undefined section with common actions to run over the selected assets.
    This might be contextual and in different occasions we might have different actions.



"""
import os
import sys
import attr
import qtawesome

from typing import List, Optional
from qtpy import QtWidgets, QtCore, QtGui

# done: combine assetBrowser and tmp files
# done: Find way to add indicators to the assetIcons (Stared, Playback, usedTimes)
#  if not possible:
#   - Right click -> Add to Favourites | Remove from Favourites
#   - ToolTip -> Times used: <1234>
#   - We dont need Playback icon
# todo: study UI layout and UX
#  - remove Qsplitter filters/details
#  - reduce search bar size --its too long; bad UX
#  - 3 main columns Left(category), Center(assets), Right(details)
#  Filters could be added to Left or Center. --make mockups to test
#  - Categories is a treeView --singleSelection --works as filter
#  Categories like HDRI, Texture, Material, Model, FX and their subChildren
#  - We can have a Filters section where we configure what assets we want
#  Size(bbox), Kind(usdMetadata), Stared, Has Thumbnail/anim, sort by usedTimes, name, date, from project
#  - Study how to separate Tags, Categories, Filters (define what are them, at leas internally) Do we need them?
# todo: separate AssetBrowser into multiple well-defined widgets.
# todo: add rightClick actions like:
#  - Right click -> Add to Favourites | Remove from Favourites
# done: Study AssetData() class and what stuff we need
#  - id, name, media, payload/s, (usdMetadata) + size, usedTimes, date, project, tags

ICON_SIZE = 120
MIN_ICON_SIZE = 80
MAX_ICON_SIZE = 500
AUTO_SEARCH_TIMEOUT = 5
ITEM_DATA_ROLE = QtCore.Qt.UserRole + 1

ROOT = os.path.expandvars('${USERPROFILE}/Desktop/_Repos/TeideTools/assetcreator')
IMAGE = ROOT + "/resources/emptyImage.png"
FAVOURITE = ROOT + "/resources/star.svg"
NO_FAVOURITE = ROOT + "/resources/star-o.svg"
APPLE = ROOT + "/resources/Apple.jpg"
BOWL = ROOT + "/resources/Bowl.jpg"
FRUITBOWL = ROOT + "/resources/Fruitbowl.jpg"


def resizeIcon(icon: QtGui.QIcon, size: int) -> QtGui.QIcon:
    # todo: move to QT lib?
    new_size = QtCore.QSize(size, size)
    new_pixmap = icon.pixmap(new_size)
    return QtGui.QIcon(new_pixmap)


@attr.s(repr=False)
class Asset(object):
    name = attr.ib(type=str)
    author = attr.ib(type=Optional[str], default=None)
    category = attr.ib(type=Optional[str], default=None)
    tags = attr.ib(type=Optional[List[str]], default=None)
    thumbnail = attr.ib(type=Optional[str], default=None)
    used_times = attr.ib(type=int)
    payloads = attr.ib(type=Optional[List[object]], default=None)
    favourite = attr.ib(type=bool, default=False)

    def __repr__(self):
        return "%s('%s')" % (self.__class__.__name__, self.name)

    @used_times.default
    def used_timesDefault(self) -> int:
        """
        Generate random used times
        """
        import random
        value = random.Random().randint(0, 150)
        return value


class ModelAsset(Asset):
    kind = attr.ib(type=Optional[str], default=None)
    size = attr.ib(type=Optional[int], default=None)  # approximate bbox size

    @property
    def hasRig(self) -> bool:
        # todo: evaluate if has a rigged payload.
        return False


DEMO_DATA = [Asset(name=str(N), thumbnail=None) for N in range(16)]
DEMO_DATA.extend([
    Asset(name='Apple', thumbnail=APPLE, used_times=0, category='Prop'),
    Asset(name='Bowl', thumbnail=BOWL, favourite=True, used_times=8, category='Prop'),
    Asset(name='FruitBowl', thumbnail=FRUITBOWL, used_times=500123, category='Prop/Assembly')]
)


class CategoryModel(QtCore.QAbstractItemModel):
    undefined_category = 'Undefined'

    def __init__(self, assets, parent=None):
        super(CategoryModel, self).__init__(parent)
        self.rootItem = QtGui.QStandardItem()
        self.setupModelData(assets, self.rootItem)

    def setupModelData(self, assets, parent):
        parent.appendRow(QtGui.QStandardItem('All'))
        category_map = {}
        for asset in assets:
            if asset.category:
                categories = asset.category.split('/')
            else:
                categories = [self.undefined_category]
            current_item = parent
            for category in categories:
                if category == '':
                    category = self.undefined_category
                if category not in category_map:
                    category_item = QtGui.QStandardItem(category)
                    category_map[category] = category_item
                    current_item.appendRow(category_item)
                current_item = category_map[category]

    def rowCount(self, parentIndex):
        if not parentIndex.isValid():
            return self.rootItem.rowCount()
        return parentIndex.internalPointer().rowCount()

    def columnCount(self, parentIndex):
        return 1  # Only one column

    def data(self, index, role):
        if not index.isValid():
            return None

        item = index.internalPointer()

        if role == QtCore.Qt.DisplayRole:
            return item.text()

    def index(self, row, column, parentIndex):
        if not self.hasIndex(row, column, parentIndex):
            return QtCore.QModelIndex()

        if not parentIndex.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parentIndex.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QtCore.QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)


class CategoriesWdg(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setFixedWidth(250)

        # Main Layout
        self.asset_filters_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.asset_filters_layout)

        # Categories Label
        title_font = QtGui.QFont()
        title_font.setPixelSize(25)
        categories_label = QtWidgets.QLabel('Categories')
        categories_label.setFont(title_font)
        self.asset_filters_layout.addWidget(categories_label)

        # TreeView
        model = CategoryModel(DEMO_DATA)
        treeView = QtWidgets.QTreeView(parent=self)
        treeView.setHeaderHidden(True)
        treeView.setModel(model)
        self.asset_filters_layout.addWidget(treeView)


class AssetItemDelegate(QtWidgets.QStyledItemDelegate):
    def __init__(self, parent=None):
        super().__init__(parent)
        # Calculate and store the favorite icon pixmap
        self._indicator_size = 16
        self._indicator_margin = 5
        self._indicator_font_size = 8
        self._indicator_color = QtGui.QColor('red')  # Red color
        self.favorite_pix = QtGui.QIcon(FAVOURITE).pixmap(self._indicator_size, self._indicator_size)
        self.nofavorite_pix = QtGui.QIcon(NO_FAVOURITE).pixmap(self._indicator_size, self._indicator_size)

    def paint(self, painter, option, index):
        super().paint(painter, option, index)
        itemData = index.model().data(index, ITEM_DATA_ROLE)

        # Calculate the position for the favorite icon
        icon_x = option.rect.right() - self._indicator_size - self._indicator_margin
        icon_y = option.rect.top() + self._indicator_margin

        # Draw the favorite icon using the precalculated pixmap
        fav_pix = self.favorite_pix if itemData.favourite else self.nofavorite_pix
        painter.drawPixmap(icon_x, icon_y, fav_pix)

        if itemData.used_times == 0:
            return
        # Create a font for the text
        font = QtGui.QFont()
        font.setBold(True)
        font.setPointSize(self._indicator_font_size)
        painter.setFont(font)
        painter.setPen(self._indicator_color)

        # measured text width
        used_times_text = str(itemData.used_times)
        metrics = QtGui.QFontMetrics(font)
        text_width = metrics.horizontalAdvance(used_times_text)
        # Adding a margin because the metrix are not precise enough after 3+ digits.
        text_margin = (2 if len(used_times_text) <= 3 else -5)

        # Calculate the position for the text using the measured text width
        text_x = option.rect.right() - text_width - text_margin
        text_y = icon_y + fav_pix.height() + self._indicator_size

        # Draw the used_times text
        painter.drawText(text_x, text_y, used_times_text)


class AssetListView(QtWidgets.QListView):
    """
    A QListView that scales it's grid size to ensure the same number of
    columns are always drawn.
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.icon_size = ICON_SIZE
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

    def resizeEvent(self, event):
        """
        Re-implemented to re-calculate the grid size to provide scaling icons

        Parameters
        ----------
        event : QtCore.QEvent
        """
        # The minus 30 ensures we don't end up with an item width that
        # can't be drawn the expected number of times across the view without
        # being wrapped. Without this, the view can flicker during resize
        width = self.viewport().width() - 150
        tileWidth = width / (width/self.icon_size)

        iconWidth = int(tileWidth * 0.8)
        tileWidth = int(tileWidth)

        self.setGridSize(QtCore.QSize(tileWidth, tileWidth))
        self.setIconSize(QtCore.QSize(iconWidth, iconWidth))

        return super().resizeEvent(event)


class AssetListModel(QtCore.QAbstractItemModel):
    def __init__(self, data=None, parent=None):
        super().__init__(parent)
        self._data: List[Asset] = data

    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self._data) if self._data else 0

    def columnCount(self, parent=QtCore.QModelIndex()):
        return 1  # Assuming a single column

    def index(self, row, column, parent=QtCore.QModelIndex()):
        if 0 <= row < len(self._data) and column == 0:
            return self.createIndex(row, column)
        return QtCore.QModelIndex()

    def parent(self, index):
        return QtCore.QModelIndex()

    def data(self, index, role):
        if not index.isValid():
            return None
        if not self._data:
            return None
        itemData = self._data[index.row()]

        if role == QtCore.Qt.DisplayRole:
            return itemData.name
        elif role == QtCore.Qt.DecorationRole:
            if itemData.thumbnail:
                icon_path = itemData.thumbnail
            else:
                icon_path = IMAGE
            pix = QtGui.QPixmap(icon_path)
            icon = QtGui.QIcon(pix)
            return resizeIcon(icon, 600)
        elif role == ITEM_DATA_ROLE:
            return itemData
        return None

    def refreshData(self, newData):
        # type: (List[Asset]) -> None
        self.beginResetModel()
        self._data = newData
        self.endResetModel()


class AssetListWdg(QtWidgets.QWidget):
    def __init__(self, model, parent):
        QtWidgets.QWidget.__init__(self, parent)
        self._filterTimer = QtCore.QTimer(self)
        self._filterTimer.setSingleShot(True)
        self._filterTimer.setInterval(AUTO_SEARCH_TIMEOUT)
        self._filterTimer.timeout.connect(self._updateFilter)

        self._listModel = model  # AssetListModel()

        self._proxyModel = QtCore.QSortFilterProxyModel()
        self._proxyModel.setSourceModel(self._listModel)
        self._proxyModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)

        self._listView = AssetListView(self)
        self._listView.setUniformItemSizes(True)
        self._listView.setViewMode(QtWidgets.QListView.IconMode)
        self._listView.setModel(self._proxyModel)
        self._listView.setItemDelegate(AssetItemDelegate())
        self._listView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        self._lineEdit = QtWidgets.QLineEdit(self)
        self._lineEdit.setAlignment(QtCore.Qt.AlignCenter)
        self._lineEdit.setPlaceholderText('Search asset')
        self._lineEdit.textChanged.connect(self._triggerDelayedUpdate)
        self._lineEdit.returnPressed.connect(self._triggerImmediateUpdate)

        lyt = QtWidgets.QHBoxLayout()
        lyt.setContentsMargins(0, 0, 0, 0)
        lyt.addWidget(self._lineEdit)

        searchBarFrame = QtWidgets.QFrame(self)
        searchBarFrame.setLayout(lyt)

        self.icon_size_slider = QtWidgets.QSlider(parent=self)
        self.icon_size_slider.setOrientation(QtCore.Qt.Orientation.Horizontal)
        self.icon_size_slider.setValue(ICON_SIZE)
        self.icon_size_slider.setMinimum(MIN_ICON_SIZE)
        self.icon_size_slider.setMaximum(MAX_ICON_SIZE)
        self.icon_size_slider.setToolTip('Icon size')
        self.icon_size_slider.valueChanged.connect(self._updateSlider)

        lyt = QtWidgets.QVBoxLayout()
        lyt.addWidget(searchBarFrame)
        lyt.addWidget(self.icon_size_slider)
        lyt.addWidget(self._listView)

        self.setLayout(lyt)

        QtWidgets.QShortcut(
            QtGui.QKeySequence("Ctrl+F"),
            self,
            self._lineEdit.setFocus,
        )

        # self._lineEdit.setFocus()

        geo = self.geometry()

        # QApplication.desktop() has been removed in Qt 6.
        # Instead, QGuiApplication.screenAt(QPoint) is supported
        # in Qt 5.10 or later.
        try:
            screen = QtGui.QGuiApplication.screenAt(QtGui.QCursor.pos())
            centerPoint = screen.geometry().center()
        except AttributeError:
            desktop = QtWidgets.QApplication.desktop()
            screen = desktop.screenNumber(desktop.cursor().pos())
            centerPoint = desktop.screenGeometry(screen).center()

        geo.moveCenter(centerPoint)
        self.setGeometry(geo)

    def _updateFilter(self):
        """
        Update the string used for filtering in the proxy model with the
        current text from the line edit.
        """
        reString = ""

        searchTerm = self._lineEdit.text()
        if searchTerm:
            reString += ".*%s.*$" % searchTerm

        # QSortFilterProxyModel.setFilterRegExp has been removed in Qt 6.
        # Instead, QSortFilterProxyModel.setFilterRegularExpression is
        # supported in Qt 5.12 or later.
        try:
            self._proxyModel.setFilterRegularExpression(reString)
        except AttributeError:
            self._proxyModel.setFilterRegExp(reString)

    def _triggerDelayedUpdate(self):
        """
        Reset the timer used for committing the search term to the proxy model.
        """
        self._filterTimer.stop()
        self._filterTimer.start()

    def _triggerImmediateUpdate(self):
        """
        Stop the timer used for committing the search term and update the
        proxy model immediately.
        """
        self._filterTimer.stop()
        self._updateFilter()

    def _updateSlider(self):
        self._listView.icon_size = self.icon_size_slider.value()
        event = QtGui.QResizeEvent(self._listView.size(), self._listView.size())
        self._listView.resizeEvent(event)


class AssetBrowser(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.gDATA = None  # type: Optional[List[Asset]]
        self.init_ui()
        self.populate_ui()
        # self.connect_signals()

    def init_ui(self):
        self.setWindowTitle("Asset Browser")
        self.setGeometry(100, 100, 1200, 600)
        main_layout = QtWidgets.QHBoxLayout(self)

        # Left Widget: Categories
        categories_wdg = CategoriesWdg()

        main_layout.addWidget(categories_wdg)

        # Center Widget: Asset Grid and Search Bar
        self.asset_list_model = AssetListModel()
        self.asset_list_wdg = AssetListWdg(model=self.asset_list_model, parent=self)
        main_layout.addWidget(self.asset_list_wdg)

        # Right Widget: Asset Details
        # right_widget = RightWidget(self)
        # splitter.addWidget(right_widget)

    def populate_ui(self):
        newData = self.gather_data()
        self.asset_list_model.refreshData(newData)
        assets_number = str(len(self.asset_list_wdg._listModel._data)) + '+' if self.asset_list_wdg._listModel._data else ''
        self.asset_list_wdg._lineEdit.setPlaceholderText('Search {0} asset'.format(assets_number))

    def gather_data(self):
        # DEMO_DATA = [Asset(name=str(N), thumbnail=None) for N in range(16)]
        # DEMO_DATA.extend([
        #     Asset(name='Apple', thumbnail=APPLE, used_times=0, category='Prop'),
        #     Asset(name='Bowl', thumbnail=BOWL, favourite=True, used_times=8, category='Prop'),
        #     Asset(name='FruitBowl', thumbnail=FRUITBOWL, used_times=500123, category='Prop/Assembly')]
        # )
        import os
        import glob

        # Stress testing data
        image_directory = os.path.expandvars('P:/global_library/Models/_imageScrubber')
        image_directory = os.path.expandvars('P:/global_library/HDRI/HDRIHeaven')

        if not os.path.isdir(image_directory):
            image_directory = os.path.expandvars('${USERPROFILE}/Desktop/_Repos/TeideTools/assetcreator')

        # Use glob to gather all image files in the directory
        image_files = glob.glob(os.path.join(image_directory, '*/*.png'))
        # Print the file paths of all images
        DEMO_DATA = []
        for image_file in image_files:
            name = image_file.replace('\\', '/').rsplit('/', 1)[-1].rsplit('.', 1)[0]
            asset = Asset(name=str(name), thumbnail=image_file)
            DEMO_DATA.append(asset)
        return DEMO_DATA


class RightWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.asset_details_layout = QtWidgets.QVBoxLayout()
        self.setLayout(self.asset_details_layout)
        self.asset_details_gbox = QtWidgets.QGroupBox('Details')
        self.asset_details_layout.addWidget(self.asset_details_gbox)
        # Add your asset info widgets here


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    qtawesome.dark(app)
    browser = AssetBrowser()
    browser.show()
    sys.exit(app.exec_())
