import os
import sys
from typing import Union, Optional, List, Dict

import qtawesome
from qtpy import QtCore, QtGui, QtWidgets


IMAGE = os.path.expandvars('${USERPROFILE}/Desktop/_Repos/TeideTools/assetcreator/resources/emptyImage.png')


class BaseIcon(QtWidgets.QAbstractButton):
    def __init__(self, icons: Union[List[QtGui.QIcon], QtGui.QIcon], parent=None):
        super(BaseIcon, self).__init__(parent)
        if isinstance(icons, QtGui.QIcon):
            icons = [icons]
        self.icons = icons

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        option = QtWidgets.QStyleOptionButton()
        option.initFrom(self)
        icon = self.icons[0]
        icon.paint(painter, option.rect, QtCore.Qt.AlignCenter)


class BaseToggleIconBtn(BaseIcon):
    def __init__(self, icons: Union[List[QtGui.QIcon], QtGui.QIcon], parent=None):
        super(BaseToggleIconBtn, self).__init__(icons, parent)
        self.checked = False

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        option = QtWidgets.QStyleOptionButton()
        option.initFrom(self)
        if self.isChecked():
            option.state |= QtWidgets.QStyle.State_On
        else:
            option.state |= QtWidgets.QStyle.State_Off

        icon = self.icons[0] if not self.checked else self.icons[1]
        icon.paint(painter, option.rect, QtCore.Qt.AlignCenter)

    def mousePressEvent(self, event):
        self.setChecked(not self.isChecked())
        self.clicked.emit()

    def isChecked(self):
        return self.checked

    def setChecked(self, checked):
        if self.checked != checked:
            self.checked = checked
            self.update()


class TimesUsedIcon(BaseIcon):
    def __init__(self, times_used: str, parent=None):
        super().__init__(self.createTimesUsedIcon(times_used), parent)

    @staticmethod
    def createTimesUsedIcon(times_used: str):
        font = QtGui.QFont()
        font.setPixelSize(22)  # Set the desired font size
        text_color = QtGui.QColor('gray')  # Set text color to blue

        # Create a transparent pixmap with a size that fits your text
        pixmap = QtGui.QPixmap(40, 40)  # Adjust the size as needed
        pixmap.fill(QtCore.Qt.transparent)

        # Create a painter to draw on the pixmap
        painter = QtGui.QPainter(pixmap)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.setRenderHint(QtGui.QPainter.TextAntialiasing)
        painter.setFont(font)
        painter.setPen(text_color)

        # Calculate the position to center the text
        text_rect = pixmap.rect()
        metrics = painter.fontMetrics()
        text_width = metrics.width(times_used)
        text_height = metrics.height()
        x = (text_rect.width() - text_width) // 2
        y = (text_rect.height() + text_height) // 2

        # Draw the text onto the pixmap
        painter.drawText(x, y, times_used)

        painter.end()  # Finish painting

        icon = QtGui.QIcon(pixmap)
        return icon


class BaseImageWidget(QtWidgets.QWidget):
    default_image = IMAGE
    minimum_size = 150
    icon_size = 24

    def __init__(self):
        super().__init__()
        self.image = QtGui.QPixmap(self.default_image) if self.default_image else None

        self.initUI()
        self.connect_signals()

    def initUI(self):
        self.setMinimumSize(self.minimum_size, self.minimum_size)
        # self.setGeometry(100, 100, 400, 400)

    def connect_signals(self):
        raise NotImplementedError

    def paintEvent(self, event):
        if not self.image:
            aspect_ratio = 1
        else:
            aspect_ratio = self.image.width() / self.image.height()
        width = self.width()
        height = self.width() / aspect_ratio
        if height > self.height():
            height = self.height()
            width = self.height() * aspect_ratio

        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.SmoothPixmapTransform)
        painter.drawPixmap((self.width() - width) / 2, (self.height() - height) / 2, width, height, self.image)


class ImagePickerWidget(BaseImageWidget):
    """AssetCreator Thumbnail picker"""

    def initUI(self):
        super().initUI()

        # Load/Change Image
        self.button_change = QtWidgets.QPushButton(icon=qtawesome.icon("fa.folder-o"),
                                                   parent=self
                                                   )
        self.button_change.setToolTip('Load image')
        self.button_change.setFlat(True)

        # Remove Image
        self.button_remove = QtWidgets.QPushButton(icon=qtawesome.icon("fa.trash-o"),
                                                   parent=self
                                                   )
        self.button_remove.setToolTip('Remove image')
        self.button_remove.setFlat(True)

    def connect_signals(self):
        # Load/Change Image
        self.button_change.clicked.connect(self.changeImage)
        # Remove Image
        self.button_remove.clicked.connect(self.removeImage)

    def changeImage(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.ReadOnly
        file_dialog = QtWidgets.QFileDialog.getOpenFileName(
            self, "Select Background Image", "", "Images (*.png *.jpg *.bmp *.gif *.jpeg *.tiff)", options=options
        )

        if file_dialog[0]:
            self.image = QtGui.QPixmap(file_dialog[0])
            self.update()

    def removeImage(self):
        self.image = QtGui.QPixmap(self.default_image) if self.default_image else None
        self.update()

    def resizeEvent(self, event):
        if self.image:
            # Update button sizes and positions
            button_size = self.icon_size  # Adjust button size as needed
            self.button_change.setGeometry(0, 0, button_size, button_size)
            self.button_remove.setGeometry(self.width() - button_size, 0, button_size, button_size)
            self.update()


class AssetWidget(BaseImageWidget):
    """AssetBrowser asset icon shown in grid"""

    def initUI(self):
        super().initUI()

        # Asset Name
        font = QtGui.QFont()
        font.setPixelSize(20)
        # self.name_label = QtWidgets.QLabel(text='SuperChuiliASset', parent=self)
        self.name_label = QtWidgets.QLabel(text='FruitBowl', parent=self)
        self.name_label.setFont(font)

        # Times used
        font = QtGui.QFont()
        font.setPixelSize(10)
        self.used_times_icon = QtWidgets.QLabel(text='0', parent=self)
        self.used_times_icon.setAlignment(QtCore.Qt.AlignRight)
        self.used_times_icon.setEnabled(False)
        self.used_times_icon.setToolTip('Times used')
        self.used_times_icon.setFont(font)

        # Animated Media
        media_icon_size = 12
        media_icon = qtawesome.icon('fa5s.play')

        def resizeIcon(icon: QtGui.QIcon, size: int) -> QtGui.QIcon:
            # todo: move to QT lib?
            new_size = QtCore.QSize(size, size)
            new_pixmap = icon.pixmap(new_size)
            return QtGui.QIcon(new_pixmap)

        self.media_icon = BaseIcon(icons=resizeIcon(media_icon, media_icon_size),
                                   parent=self)
        self.media_icon.setToolTip('Has animated media')

        # Favourite
        fav_icons = [qtawesome.icon('fa.star-o'), qtawesome.icon('fa.star')]
        self.favourite_icon = BaseToggleIconBtn(icons=fav_icons, parent=self)
        self.favourite_icon.setToolTip('Add to favourites')

    def connect_signals(self):
        # Remove Image
        self.favourite_icon.clicked.connect(self.toggle_favourite)
        pass

    def toggle_favourite(self):
        state = self.favourite_icon.isChecked()
        print('pseudo {0} Favourites'.format('adding to' if state else 'removing from'))

    def resizeEvent(self, event):
        text = self.name_label.text()
        available_width = self.width()

        # Determine a maximum font size for the label
        max_font_size = 45  # Set your desired maximum font size

        # Create a QFontMetrics object for font size calculations
        metrics = QtGui.QFontMetrics(self.name_label.font())

        # Decrease the font size to fit the available width
        current_font_size = max_font_size
        while metrics.width(text) > available_width and current_font_size > 0:
            current_font_size -= 1
            self.name_label.setFont(QtGui.QFont(self.name_label.font().family(), current_font_size))

        label_height = metrics.height()

        # Calculate the X position to center the label horizontally
        label_width = metrics.width(text)
        x_position = (available_width - label_width) // 2

        # Set the geometry for the name_label
        self.name_label.setGeometry(x_position, self.height() - label_height, label_width, label_height)

        text_width = self.used_times_icon.size().width()
        if self.image:
            # Update button sizes and positions
            text_margin = 4
            button_size = self.icon_size  # Adjust button size as needed
            # self.name_label.setGeometry(0, name_size.width(), self.height() - name_size.height(), name_size.height())
            # self.name_label.setGeometry(0, self.height()-name_heigh, name_width, name_heigh)
            self.used_times_icon.setGeometry(self.width()-text_margin - text_width, button_size*2, text_width, button_size)
            self.media_icon.setGeometry(self.width() - button_size, button_size, button_size, button_size)
            self.favourite_icon.setGeometry(self.width() - button_size, 0, button_size, button_size)
            self.update()

    def setUsedTimes(self, value):
        if isinstance(value, int):
            value = str(value)
        if isinstance(value, float):
            value = str(value)
        self.used_times_icon.setText(value)


def main():
    app = QtWidgets.QApplication(sys.argv)
    qtawesome.dark(app)
    window = QtWidgets.QMainWindow()
    widget = AssetWidget()
    window.setCentralWidget(widget)
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
